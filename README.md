### Python 3 implementation of Ramer–Douglas–Peucker algorithm.

*Work in progress ...*

The purpose of the algorithm is, given a curve composed of line segments (which is also called a Polyline in some contexts), to find a similar curve with fewer points.

Reduce points:

![Algorithm rdp](images/Douglas-Peucker_animated.gif)

Parameter epsilon precision effect:

![RDP_varying_epsilon](images/RDP_varying_epsilon.gif)


#### USAGE
rdp.py <tsv_file> <tolerance> [<result_tsv_file>]

**Example**: 
```bash
$ rdp.py sample/activity_66308030.tsv 0.00001 /tmp/reduced.tsv
```

-------------------

+Info: https://en.wikipedia.org/wiki/Ramer-Douglas-Peucker_algorithm
