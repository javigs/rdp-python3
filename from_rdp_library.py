import csv
from functools import partial

from rdp import rdp
from time import time

# print(rdp([[1, 1], [2, 2], [3, 3], [4, 4]], algo="rec"))
ini = time()
points = open('samples/activity_66308030.tsv')
next(points)
data = [list(map(float, row)) for row in csv.reader(points, delimiter="\t", )]
elapsed = time() - ini
print("Tiempo lectura datos a memoria: ", elapsed)

ini = time()
reduced = rdp(data, epsilon=0.0001)
elapsed = time() - ini

print("Tiempo RDP: ", elapsed)
print("Original: ", len(data))
print("Reducido: ", len(reduced))

